'use strict';
/**
 * @ngdoc overview
 * @name bilApp
 * @description
 * # ngApp
 * 
 * v0.0.1   
 *
 * Main module of the application.
 */
angular.module('ngApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
]).config(["$routeProvider", function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/exm02/6-1.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/6-2', {
            templateUrl: 'views/exm02/6-2.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/6-3', {
            templateUrl: 'views/exm02/6-3.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/6-4', {
            templateUrl: 'views/exm02/6-4.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/6-5', {
            templateUrl: 'views/exm02/6-5.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/6-6', {
            templateUrl: 'views/exm02/6-6.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/tst', {
            templateUrl: 'views/tst.html',
            controllerAs: 'main2'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
